﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class Node
{
	private List<Node> parents;
	private List<Node> children;
	private List<Node> elements;
	private Vector3 position;
	private GameObject gameObject;
	private int height;
	private int width;
	private int depth;
	private Guid uuid;

	/**
	* Constructor
	*
	* NOTE: The gameObject is currently hardcoded. This will be dynamic
	*		as soon as the data driven team is done doing their stuff
	*/
	public Node(Vector3 position, Mesh nodeMesh) //, GameObject gameObject)
	{
		this.init (position);

		//Add mesh filter to the gameobject
		this.gameObject.AddComponent<MeshFilter> ().mesh = nodeMesh;

		//Set the name for the mesh equel to the unique id of the node
		nodeMesh.name = GetUniqueIdentifier();

		//Create bounds
		var b = new Bounds ();
		b.size = new Vector3(this.gameObject.transform.localScale.x, this.gameObject.transform.localScale.y, 100);
		nodeMesh.bounds = b;


		//Create material for mesh renderer and give it unique ID of the node

		//Add transparent shader to make to view content inside the node
		var m = new Material (Shader.Find("Transparent/Diffuse"));
		m.color = Color.white;
		m.name = GetUniqueIdentifier ();

		//Add mesh renderer to the gameobject and attach created material to the mesh renderer
		this.gameObject.AddComponent<MeshRenderer> ().material = m;

		this.gameObject.tag = "Node";

	}

	public Node(Vector3 position, Mesh nodeMesh, Component component)
	{
		this.init (position);

		// Add logic for data driven stuff.
		// Now a hardcoded text field
		Text text = gameObject.AddComponent<Text>();
		text.text = "Dit is een tekstveldje";
		Font ArialFont = (Font)Resources.GetBuiltinResource(typeof(Font), "Arial.ttf");
		text.font = ArialFont;
		text.fontSize = 3;
		text.color = Color.black;
		text.transform.localScale = new Vector3 (1,1);

		//Image image = gameObject.AddComponent<Image> ();



	}

	//Empty constructor to make FilterNodes() work since it always has to return a Node;
	public Node(){
	}

	public void init(Vector3 position)
	{
		//Give unique ID to the node
		uuid = Guid.NewGuid ();

		//Set position z to 100, otherwise object is not visible to the camera 
		position.z = 100;
		this.position = position;

		//Create the gameobject based on the node and give unique ID as name
		this.gameObject = new GameObject(GetUniqueIdentifier());
		this.gameObject.transform.position = GetPosition ();
		this.gameObject.transform.rotation = Quaternion.identity;
		this.gameObject.transform.SetParent (GameObject.Find ("Canvas").transform);

		//Lists to store parents, children and elements
		parents = new List<Node>();
		children = new List<Node>();
		elements = new List<Node>();


		//Add ContentSizeFitter so that the collider adjusts to content
		this.gameObject.AddComponent<ContentSizeFitter>().horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
		this.gameObject.GetComponent<ContentSizeFitter> ().verticalFit = ContentSizeFitter.FitMode.PreferredSize;

		//Add box collider to the gameobject
		this.gameObject.AddComponent<BoxCollider> ();

		//Set the scale of the gameobject
		this.gameObject.transform.localScale = new Vector3 (100,100,100);

		//Give the box collider empty PhysicMaterial
		this.gameObject.GetComponent<BoxCollider> ().material = new PhysicMaterial();

		//Add Drag component to enable dragging, gameobject requires Mesh filter, Mesh Renderer and a Box Collider in order to work
		this.gameObject.AddComponent<Drag>();


	}


	/**
	* Add parent to list
	*/
	public void AddParent(Node parent)
	{
		parents.Add(parent);
	}

	/**
	* Add child to list
	*/
	public void AddChild(Node child)
	{
		children.Add(child);
	}

	/**
	* Add element to list
	*/
	public void AddElement(Node element)
	{
		elements.Add(element);
		element.GetGameObject ().transform.SetParent (this.GetGameObject ().transform);
	}

	/**
	* Set position
	*/
	public void SetPosition(Vector3 position)
	{
		this.position = position;
	}

	/**
	* Set GameObject
	*/
	public void SetGameObject(GameObject gameObject)
	{

		this.gameObject = gameObject;

	}

	/**
	* Set height
	*/
	public void SetHeight(int height)
	{
		this.height = height;
	}

	/**
	* Set width
	*/
	public void SetWidth(int width)
	{
		this.width = width;
	}

	/**
	* Set depth
	*/
	public void SetDepth(int depth)
	{
		this.depth = depth;
	}

	/**
	* Return list with all parents
	*/
	public List<Node> GetParents()
	{
		return parents;
	}

	/**
	* Return list with all children
	*/
	public List<Node> GetChildren()
	{
		return children;
	}

	/**
	* Return list with all elements
	*/
	public List<Node> GetElements()
	{
		return elements;
	}

	/**
	* Return position
	*/
	public Vector3 GetPosition()
	{
		return position;
	}

	/**
	* Return GameObject
	*/
	public GameObject GetGameObject()
	{
		return gameObject;
	}

	/**
	* Return height
	*/
	public int GetHeight()
	{
		return height;
	}

	/**
	* Return width
	*/
	public int GetWidth()
	{
		return width;
	}

	/**
	* Return depth
	*/
	public int GetDepth()
	{
		return depth;
	}

	//Return unique ID to string
	public string GetUniqueIdentifier(){
		return uuid.ToString ();
	}
}