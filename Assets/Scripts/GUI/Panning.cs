using UnityEngine;
using System.Collections;

public class Panning : MonoBehaviour {
	private Vector3 ResetCamera;
	private Vector3 Origin;
	private Vector3 Difference;
	private bool dragFunction=false;

	void LateUpdate () {
		if (Drag.dragging == false) {
			if (Input.GetMouseButton (0)) {
				Difference = (Camera.main.ScreenToWorldPoint (Input.mousePosition)) - Camera.main.transform.position;
				if (dragFunction == false) {
					dragFunction = true;
					Origin = Camera.main.ScreenToWorldPoint (Input.mousePosition);
				}
			} else {
				dragFunction = false;
			}
			if (dragFunction == true) {
				Camera.main.transform.position = Origin - Difference;
			}
		}
	}
}