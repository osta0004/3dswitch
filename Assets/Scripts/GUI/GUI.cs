﻿using UnityEngine;
using System.Collections.Generic;
using System;


public class GUI : MonoBehaviour {

	public List<Node> nodes;
	private int nr;
	public Mesh nodeMesh;
	// Use this for initialization
	void Start () {
		nodes = new List<Node>();
		nr = 0;
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown(1))
		{
			Camera camera = Camera.main;
			Vector3 pos = camera.ScreenToWorldPoint(Input.mousePosition);


			//Create Raycast to detect the gameobject that has been clicked
			RaycastHit hit;

			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);

			//Check if the clicked gameObject has tag "Node"
			if(Physics.Raycast(ray, out hit) && hit.collider.gameObject.tag == "Node"){
					//If node is clicked add content element
					Node node = new Node (pos, nodeMesh, new Component());

					//Filter all nodes to find the object that is attached to the gameObject
					FilterNodes(hit.collider.gameObject.name).AddElement(node);

			} else{
				//If canvas is clicked create new super node
				Node superNode = new Node (pos, nodeMesh);
				nodes.Add(superNode);
				Node node = new Node (pos, nodeMesh, new Component());
				superNode.AddElement (node);
			}
		}
	}

	//Filter all nodes id's with the name of the gameobject to find the correct Node Object
	public Node FilterNodes(string id){
		var list = GameObject.Find ("Canvas").GetComponent<GUI>().nodes;
		for (int i = 0; i <= list.Count; i++) {
			if (id == list [i].GetUniqueIdentifier ()) {
				return list [i];
			} 
		}
		return new Node ();
	}

}
