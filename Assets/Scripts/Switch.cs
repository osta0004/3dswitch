﻿using UnityEngine;
using System.Collections;

public class Switch : MonoBehaviour {

	public GameObject p;
	public GameObject c;

	// Use this for initialization
	void Start () {
		p.active = false;

		
	}
	
	// Update is called once per frame
	void Update () {

		//Set camera to ortographic or perspective for 2D/3D switch
		if (Input.GetKeyDown (KeyCode.F)) {
			/*if (GetComponent<Camera> ().orthographic) {
				GetComponent<Camera> ().orthographic = false;
				Debug.Log ("3D Mode");
			} else {
				GetComponent<Camera> ().orthographic = true;
				Debug.Log ("2D Mode");
			}*/


			if (p.active) {
				p.active = false;
				c.active = true;
				Debug.Log ("2D Mode");
			} else {
				p.active = true;
				c.active = false;
				Debug.Log ("3D Mode");
			}
		}
	}
}
